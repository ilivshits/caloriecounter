﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yum.Client.Api.Models.Requests
{
    public class UserDataUpdateRequest
    {
        public Guid? UserId { get; set; }
        public string UserName { get; set; }
    }
}
