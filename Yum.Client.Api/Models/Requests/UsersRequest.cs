﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yum.Auth.Common;

namespace Yum.Client.Api.Models.Requests
{
    public class UsersRequest
    {
        public Role[] Roles { get; set; } = new Role[0];
    }
}
