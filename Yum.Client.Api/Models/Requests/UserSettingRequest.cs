﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yum.Client.Api.Models.Requests
{
    public class UserSettingRequest
    {
        public int CaloriesPlan { get; set; }
    }
}
