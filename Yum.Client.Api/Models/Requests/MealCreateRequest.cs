﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yum.Client.Api.Models.Requests
{
    public class MealCreateRequest
    {
        public DateTimeOffset DateTime { get; set; }
        public int Calories { get; set; }
        public string Name { get; set; }
        public Guid? UserId { get; set; }
    }
}
