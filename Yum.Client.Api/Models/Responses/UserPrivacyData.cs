﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yum.Client.Api.Models.Responses
{
    public class UserPrivacyData
    {
        public string FullName { get; set; }
    }
}
