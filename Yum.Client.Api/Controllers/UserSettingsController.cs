﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yum.Client.Api.Models.Requests;
using Yum.Client.Services;
using Yum.Client.Services.Models;

namespace Yum.Client.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "User")]
    [ApiController]
    public class UserSettingsController : ControllerBase
    {
        private Guid UserId => Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

        private readonly UserSettingsService _userSettingsService;

        public UserSettingsController(UserSettingsService userSettingsService)
        {
            _userSettingsService = userSettingsService;
        }

        //[HttpPost]
        //public IActionResult Create([FromBody]UserSettingRequest request)
        //{            
        //    return Ok(_userSettingsService.SetCaloriesPlan(new UserSetting()
        //    {
        //        UserId = UserId,
        //        CaloriesPlan = request.CaloriesPlan
        //    }));
        //}

        [HttpPut]
        public IActionResult Update([FromBody]UserSettingRequest request)
        {
            if (request.CaloriesPlan <= 0)
            {
                return BadRequest("Calories value cannot be less than 1.");
            }

            return Ok(_userSettingsService.SetCaloriesPlan(new UserSetting()
            {
                UserId = UserId,
                CaloriesPlan = request.CaloriesPlan
            }));
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_userSettingsService.Get(UserId));
        }
    }
}
