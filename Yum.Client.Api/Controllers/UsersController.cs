﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yum.Auth.Common;
using Yum.Auth.Services;
using Yum.Client.Api.Models.Requests;
using Yum.Client.Api.Models.Responses;

namespace Yum.Client.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private Guid UserId => Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
        private Role UserRole => Enum.Parse<Auth.Common.Role>(User.FindFirstValue(ClaimTypes.Role));

        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        [Route("data")]
        public UserPrivacyData GetUserPersonalData()
        {
            return new UserPrivacyData
            {
                FullName = _userService.Get(UserId).Name
            };
        }

        [HttpGet]
        [Authorize(Roles = "Admin,UserManager")]
        [Route("data/{id}")]
        public IActionResult GetUserPersonalData(Guid id)
        {
            var user = _userService.Get(id);
            if (user.Role != Role.User && UserRole != Role.Admin)
            {
                return Forbid();
            }

            return Ok(new UserPrivacyData
            {
                FullName = user.Name
            });
        }

        [HttpGet]
        [Authorize(Roles = "Admin,UserManager")]
        [Route("")]
        public IActionResult GetUsers([FromQuery]UsersRequest usersRequest)
        {
            if (!usersRequest.Roles.Any())
            {
                return BadRequest("At least one role type has to be provided.");
            }

            if (UserRole != Role.Admin && usersRequest.Roles.Any(role => role != Role.User))
            {
                return Forbid();
            }

            return Ok(_userService.Get(usersRequest.Roles).Select(a => new 
            {
                a.Id,
                a.Email,
                a.Name,
                a.Role
            }));
        }

        [HttpPut]
        [Route("data")]
        [Authorize]
        public IActionResult UpdateUserData([FromBody]UserDataUpdateRequest userDataUpdateRequest)
        {
            if (userDataUpdateRequest.UserId.HasValue)
            {
                if (UserRole == Role.User)
                    return Forbid();
                else if (UserRole == Role.UserManager)
                {
                    var user = _userService.Get(userDataUpdateRequest.UserId.Value);

                    if (user == null)
                        return BadRequest($"User with id '{user.Id}' does not exist.");

                    if (user.Role != Role.User) 
                        return Forbid();
                }
            }

            if (string.IsNullOrEmpty(userDataUpdateRequest.UserName))
            {
                return BadRequest("Name cannot be empty.");
            }

            var userId = userDataUpdateRequest.UserId ?? UserId;

            _userService.UpdateUserData(userId, userDataUpdateRequest.UserName);

            return Ok();
        }

        [HttpDelete]
        [Authorize(Roles = "Admin,UserManager")]
        [Route("{id}")]
        public IActionResult DeleteUser(Guid id)
        {
            var user = _userService.Get(id);

            if (UserRole == Role.User)
                return Forbid();
            else if (UserRole == Role.UserManager)
            {
                if (user.Role != Role.User)
                    return Forbid();
            }

            if (user == null)
                return BadRequest($"User with id '{id}' does not exist.");

            _userService.Delete(id, UserId);

            return Ok();
        }
    }
}