﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yum.Auth.Common;
using Yum.Client.Api.Models.Requests;
using Yum.Client.Services;
using Yum.Client.Services.Models;

namespace Yum.Client.Api.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "User,Admin")]
    [ApiController]
    public class MealsController : ControllerBase
    {
        private Guid UserId => Guid.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
        private string UserRole => User.FindFirstValue(ClaimTypes.Role);

        private readonly MealService _mealService;

        public MealsController(MealService mealService)
        {
            _mealService = mealService;
        }

        [HttpPost]
        public IActionResult Create([FromBody]MealCreateRequest request)
        {
            var validationResult = ValidateMealRequest(request.UserId, request.Calories, request.Name ?? string.Empty);
            if (validationResult != null) return validationResult;

            return Ok(_mealService.Create(new Meal()
            {
                UserId = request.UserId ?? UserId,
                DateTime = request.DateTime,
                Calories = request.Calories,
                Name = request.Name
            }));
        }

        [HttpPut]
        [Route("{id}")]
        public IActionResult Update(Guid id, [FromBody]MealUpdateRequest request)
        {
            var validationResult = ValidateMealRequest(request.UserId, request.Calories, request.Name ?? string.Empty);
            if (validationResult != null) return validationResult;

            Meal meal = _mealService.Get(id);
            if (meal == null) return NotFound($"There is no meal with ID: '{id}'");

            if (meal.UserId != (request.UserId ?? UserId))
            {
                return Forbid();
            }

            _mealService.Update(id, request.Name, request.Calories, request.DateTime);

            return Ok();
        }

        [HttpGet]
        public IActionResult Get(
            DateTimeOffset? dateFrom = null,
            DateTimeOffset? dateTo = null,
            TimeSpan? timeFrom = null,
            TimeSpan? timeTo = null,
            Guid? userId = null)
        {
            var validationResult = ValidateMealRequest(userId);
            if (validationResult != null) return validationResult;

            var meals = _mealService.Get(userId ?? UserId, dateFrom, dateTo, timeFrom, timeTo);

            return Ok(meals);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(Guid id)
        {
            Meal meal = _mealService.Get(id);
            if (meal == null) return NotFound($"There is no meal with ID: '{id}'");

            if (meal.UserId != UserId && !IsUserAdmin())
            {
                return Forbid();
            }

            return Ok(meal);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult Delete(Guid id, Guid? userId = null)
        {
            var validationResult = ValidateMealRequest(userId);
            if (validationResult != null) return validationResult;

            Meal meal = _mealService.Get(id);
            if (meal == null) return NotFound($"There is no meal with ID: '{id}'");

            if (meal.UserId != UserId && !IsUserAdmin())
            {
                return Forbid();
            }

            _mealService.Delete(id);

            return Ok();
        }

        private IActionResult ValidateMealRequest(Guid? userId, int? calories = null, string name = null)
        {
            if (userId.HasValue && !IsUserAdmin())
            {
                return Forbid();
            }

            if (!userId.HasValue && IsUserAdmin())
            {
                return BadRequest("User with this role cannot manage own meals.");
            }

            if (calories.HasValue && calories.Value <= 0)
            {
                return BadRequest("Calories value cannot be less than 1.");
            }

            if (calories.HasValue && calories.Value <= 0)
            {
                return BadRequest("Calories value cannot be less than 1.");
            }

            if (name != null && name == string.Empty)
            {
                return BadRequest("Name cannot be empty.");
            }

            return null;
        }

        private bool IsUserAdmin()
        {
            return UserRole == Role.Admin.ToString();
        }
    }
}
