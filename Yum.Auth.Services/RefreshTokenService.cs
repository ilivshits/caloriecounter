﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Yum.Auth.Services.Extensions;
using Yum.Auth.Services.Models;

namespace Yum.Auth.Services
{
    public class RefreshTokenService
    {
        private Data.Context _authContext;

        public RefreshTokenService(Data.Context context)
        {
            _authContext = context;
        }

        public IEnumerable<RefreshToken> Get(Guid id)
        {
            var expiredTokens = _authContext.RefreshTokens
                .Where(a => a.UserId == id)
                .Where(a => a.Expire < DateTimeOffset.UtcNow);

            _authContext.RefreshTokens.RemoveRange(expiredTokens);
            _authContext.SaveChanges();

            return _authContext.RefreshTokens
                .Where(a => a.UserId == id)
                .Select(a => a.ToDto())
                .ToList();
        }

        public void Delete(Guid userId, string refreshToken)
        {
            var token = _authContext.RefreshTokens
                .FirstOrDefault(a => a.UserId == userId && a.Token == refreshToken);

            if (token == null) return;

            _authContext.RefreshTokens.Remove(token);
            _authContext.SaveChanges();
        }

        public RefreshToken Create(Guid userId, string refreshToken)
        {
            var entity = _authContext.RefreshTokens.Add(new Data.RefreshToken()
            {
                UserId = userId,
                Token = refreshToken,
                Expire = DateTimeOffset.UtcNow.AddMonths(1)
            });

            _authContext.SaveChanges();

            return entity.Entity.ToDto();
        }
    }
}
