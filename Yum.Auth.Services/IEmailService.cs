﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Yum.Auth.Services
{
    public interface IEmailService
    {
        void SendEmail(string email, string subject, string message);

    }
}
