﻿using System;
using System.Collections.Generic;
using System.Text;
using Yum.Auth.Common;
using Yum.Auth.Data;

namespace Yum.Auth.Services.Models
{
    public class RefreshToken
    {
        public Guid UserId { get; set; }
        public string Token { get; set; }
        public DateTimeOffset Expire { get; set; }
    }
}
