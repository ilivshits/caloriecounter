﻿using System;
using System.Collections.Generic;
using System.Text;
using Yum.Auth.Common;
using Yum.Auth.Data;

namespace Yum.Auth.Services.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; } = Role.User;
    }
}
