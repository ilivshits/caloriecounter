﻿using System;
using System.Collections.Generic;
using System.Text;
using Yum.Auth.Services.Models;

namespace Yum.Auth.Services.Extensions
{
    public static class MappingExtensions
    {
        public static User ToDto(this Data.User entity)
        {
            return new User
            {
                Id = entity.Id,
                Email = entity.Email,
                Name = entity.Name,
                Password = entity.Password,
                Role = entity.Role,
            };
        }

        public static RefreshToken ToDto(this Data.RefreshToken entity)
        {
            return new RefreshToken
            {
                UserId = entity.UserId,
                Token = entity.Token,
                Expire = entity.Expire
            };
        }
    }
}
