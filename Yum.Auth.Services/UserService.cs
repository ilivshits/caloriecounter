﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yum.Auth.Common;
using Yum.Auth.Services.Extensions;
using Yum.Auth.Services.Models;

namespace Yum.Auth.Services
{
    public class UserService
    {
        private Data.Context _authContext;

        public UserService(Data.Context context)
        {
            _authContext = context;
        }

        public User Get(Guid id)
        {
            return _authContext.Users.FirstOrDefault(u => u.Id == id)?.ToDto();
        }

        public IEnumerable<User> Get(Role[] roles = null, string email = null)
        {
            var query = _authContext.Users.AsQueryable();

            if (roles != null && roles.Any())
            {
                query = query.Where(u => roles.Contains(u.Role));
            }

            if (!string.IsNullOrEmpty(email))
            {
                query = query.Where(u => u.Email.Contains(email));
            }

            return query.Select(u => u.ToDto()).ToList();
        }

        public User Get(string email)
        {
            return _authContext.Users.FirstOrDefault(u => u.Email == email)?.ToDto();
        }

        public User Create(User user)
        {
            var existingUser = _authContext.Users.FirstOrDefault(u => u.Email == user.Email);

            if (existingUser != null) throw new InvalidOperationException("User with such Email already exist.");

            var entity = _authContext.Users.Add(new Data.User
            {
                Id = Guid.NewGuid(),
                Email = user.Email,
                Name = user.Name,
                Password = user.Password,
                Role = user.Role
            });

            _authContext.SaveChanges();

            return entity.Entity.ToDto();
        }

        public void Delete(Guid id, Guid authorId)
        {
            if (id == Guid.Parse("12002768-5F3B-45D2-BFF1-AF91DF08E366") || id == authorId)
            {
                throw new InvalidOperationException("Impossible to delete this user due to policies.");
            }

            var user = _authContext.Users.FirstOrDefault(u => u.Id == id);

            if (user == null) throw new InvalidOperationException("User with such Id does not exist.");

            _authContext.Users.Remove(user);

            _authContext.SaveChanges();
        }

        public void UpdateUserData(Guid id, string userName)
        {
            var user = _authContext.Users.FirstOrDefault(u => u.Id == id);

            if (user == null) throw new InvalidOperationException("User with such Id does not exist.");

            user.Name = userName;

            _authContext.Users.Update(user);
            _authContext.SaveChanges();
        }

        public void UpdatePassword(Guid id, string password)
        {
            var user = _authContext.Users.FirstOrDefault(u => u.Id == id);

            if (user == null) throw new InvalidOperationException("User with such Id does not exist.");

            user.Password = password;

            _authContext.Users.Update(user);
            _authContext.SaveChanges();
        }
    }
}
