﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Yum.Auth.Services
{
    public class EmailService : IEmailService
    {
        private readonly string _email;
        private readonly string _password;

        public EmailService(string email, string password)
        {
            _email = email;
            _password = password;
        }

        public void SendEmail(string email, string subject, string message)
        {
            // Credentials
            var credentials = new NetworkCredential(_email, _password);
            // Mail message
            var mail = new MailMessage()
            {
                From = new MailAddress(_email),
                Subject = subject,
                Body = message
            };
            mail.IsBodyHtml = true;
            mail.To.Add(new MailAddress(email));
            // Smtp client
            var client = new SmtpClient()
            {
                Port = 587,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = "smtp.gmail.com",
                EnableSsl = true,
            };
            client.Credentials = credentials;
            
            client.Send(mail);
        }

    }
}
