﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Yum.Auth.Common;

namespace Yum.Auth.Data
{
    public class User
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; } = Role.User;

        public virtual ICollection<RefreshToken> RefreshTokens { get; set; } = new Collection<RefreshToken>();
    }
}
