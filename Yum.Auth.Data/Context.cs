﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace Yum.Auth.Data
{
    public class Context : DbContext
    {
        private const string ConnectionStringName = "Yum.Auth";

        public DbSet<User> Users { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        
        public Context()
        {

        }

        public Context(DbContextOptions<Context> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User() {
                    Id = Guid.Parse("12002768-5F3B-45D2-BFF1-AF91DF08E366"), 
                    Email = "jarto666+admin@gmail.com", 
                    Name = "Admin",
                    Password = @"$2y$11$Ogk0bsf8vyRY7rYAeU834ueXcscDzmbi6qisGsDaHBAZ4ASExGTb6",
                    Role = Common.Role.Admin
                });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }
    }
}
