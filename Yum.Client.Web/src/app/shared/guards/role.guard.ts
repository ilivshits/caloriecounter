import { Injectable } from '@angular/core';
import { CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';
import { redirectToRootAuthenticated } from '../helpers/utils';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivateChild {

  constructor(private as: AuthService, private router: Router) { }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const roles = next.parent.data.roles;

    this.as.isAuthenticated().subscribe(res => {
      if (res) {
        const userRole = this.as.getRole().toLowerCase();

        if (!roles.includes(userRole)) {
          redirectToRootAuthenticated(userRole, this.router);
        }
      }
    });

    return true;
  }

}
