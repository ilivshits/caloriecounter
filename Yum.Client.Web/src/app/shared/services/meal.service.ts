import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Meal } from '../../dashboard/models/meal.model';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class MealService {
  private static AUTH_API = 'https://localhost:44385/api/meals/';

  constructor(private http: HttpClient) { }

  get(
  dateFrom: Date | string = null,
  dateTo: Date | string = null,
  timeFrom: string = '00:00:00',
  timeTo: string = '23:59:59',
  userId: string = null): Observable<Meal[]> {

    const params: any = {
      dateFrom: dateFrom instanceof Date ? dateFrom.toISOString() : dateFrom,
      dateTo: dateTo instanceof Date ? dateTo.toISOString() : dateTo,
      timeFrom,
      timeTo
    };

    if (userId) {
      params.userId = userId;
    }

    return this.http.get<Meal[]>(`${MealService.AUTH_API}`, { params });
  }

  getById(id, userId: string = null): Observable<Meal> {

    const params: any = {};

    if (userId) {
      params.userId = userId;
    }

    return this.http.get<Meal>(`${MealService.AUTH_API}${id}`, { params });
  }

  add(meal: Meal): Observable<Meal> {
    meal.dateTime = moment(meal.dateTime).format('YYYY-MM-DD HH:mm:ss');
    return this.http.post<Meal>(`${MealService.AUTH_API}`, meal);
  }

  update(meal: Meal): Observable<any> {
    return this.http.put(`${MealService.AUTH_API}${meal.id}`, meal);
  }

  delete(id: string, userId: string = null): Observable<any> {
    const params: any = {};

    if (userId) {
      params.userId = userId;
    }
    return this.http.delete(`${MealService.AUTH_API}${id}`, {params});
  }
}
