import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserSetting } from 'src/app/dashboard/models/user-setting.model';
import { UserSettingsService } from 'src/app/dashboard/services/user-settings.service';
import { UserPrivacyData } from '../models/user-privacy-data';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private static USERS_API = 'https://localhost:44385/api/users/';

  constructor(private http: HttpClient) { }

  getUsers(roles: string[]): Observable<User[]> {
    return this.http.get<User[]>(`${UserService.USERS_API}`, {
      params: {roles}
    });
  }

  getPrivacyData(): Observable<UserPrivacyData> {
    return this.http.get<UserPrivacyData>(`${UserService.USERS_API}data`);
  }

  getPrivacyDataByUserId(userId): Observable<UserPrivacyData> {
    return this.http.get<UserPrivacyData>(`${UserService.USERS_API}data/${userId}`);
  }

  updateData(userName, userId = null): Observable<any> {
    return this.http.put<any>(`${UserService.USERS_API}data`, {
      userId,
      userName
    });
  }

  delete(userId): Observable<any> {
    return this.http.delete<any>(`${UserService.USERS_API}${userId}`);
  }
}
