import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { Meal } from 'src/app/dashboard/models/meal.model';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { MealService } from '../../services/meal.service';
import { UserSettingsService } from 'src/app/dashboard/services/user-settings.service';
import { UpdateMealDialog } from 'src/app/shared/components/update-meal-dialog/update-meal-dialog.component';
import { AddMealDialog } from 'src/app/shared/components/add-meal-dialog/add-meal-dialog.component';
import * as moment from 'moment';

@Component({
  selector: 'app-meals-list',
  templateUrl: './meals-list.component.html',
  styleUrls: ['./meals-list.component.scss']
})
export class MealsListComponent implements OnInit {

  @Input() userId: string = null;

  @Input() meals = new MatTableDataSource<Meal>([]);

  @Input() canCreate = true;

  @Output() reload: EventEmitter<any> = new EventEmitter();

  @Input() isLoading = true;

  displayedColumns: string[] = ['name', 'cals', 'date', 'actions'];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  constructor(public dialog: MatDialog, private ms: MealService) {

  }

  ngOnInit() {
  }

  reloadData() {
    this.reload.emit();
  }

  deleteMeal(id) {
    this.isLoading = true;
    this.ms.delete(id, this.userId)
    .subscribe(res => {
      this.reloadData();
    });
  }

  openAddMealDialog(): void {
    const dialogRef = this.dialog.open(AddMealDialog, {
      width: '250px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addMeal(result.name, result.value);
      }
    });
  }

  openUpdateMealDialog(id): void {
    const dialogRef = this.dialog.open(UpdateMealDialog, {
      width: '250px',
      data: {id, userId: this.userId}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const date = moment(result.date).format(`YYYY-MM-DD ${result.time}:00`);
        this.updateMeal(result.id, result.name, result.calories, date);
      }
    });
  }

  addMeal(name, value) {
    this.isLoading = true;
    const meal = new Meal();
    meal.dateTime = moment(new Date()).toString();
    meal.name = name;
    meal.calories = value;
    meal.userId = this.userId;
    this.ms.add(meal).subscribe(res => {
      this.reloadData();
    });
  }

  updateMeal(id, name, value, dateTime) {
    this.isLoading = true;
    const meal = new Meal();
    meal.dateTime = dateTime;
    meal.id = id;
    meal.name = name;
    meal.calories = value;
    meal.userId = this.userId;
    this.ms.update(meal).subscribe(res => {
      this.reloadData();
    });
  }

}
