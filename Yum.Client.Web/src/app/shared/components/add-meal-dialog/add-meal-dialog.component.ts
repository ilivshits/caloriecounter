import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MealService } from '../../services/meal.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-meal-dialog',
  templateUrl: './add-meal-dialog.component.html',
  styleUrls: ['./add-meal-dialog.component.scss']
})
// tslint:disable-next-line: component-class-suffix
export class AddMealDialog implements OnInit {
  mealForm: FormGroup;

  createForm() {

    const form: any = {
      name: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      value: ['', Validators.compose([Validators.min(1), Validators.max(99999), Validators.required])]
    };

    this.mealForm = this.fb.group(form);
  }

  constructor(
    private fb: FormBuilder, private ms: MealService,
    public dialogRef: MatDialogRef<AddMealDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.createForm();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
