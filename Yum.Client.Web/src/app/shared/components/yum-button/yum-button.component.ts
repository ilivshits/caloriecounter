import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-yum-button',
  templateUrl: './yum-button.component.html',
  styleUrls: ['./yum-button.component.scss']
})
export class YumButtonComponent implements OnInit {

  @Input() outline = false;
  @Input() text = '';
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onClick: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onClickHandler() {

  }

}
