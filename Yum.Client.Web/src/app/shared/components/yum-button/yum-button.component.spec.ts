import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YumButtonComponent } from './yum-button.component';

describe('YumButtonComponent', () => {
  let component: YumButtonComponent;
  let fixture: ComponentFixture<YumButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YumButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YumButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
