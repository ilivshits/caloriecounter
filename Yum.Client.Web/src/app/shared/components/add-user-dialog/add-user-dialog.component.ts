import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MealService } from 'src/app/shared/services/meal.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddMealDialog } from 'src/app/shared/components/add-meal-dialog/add-meal-dialog.component';

@Component({
  selector: 'app-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.scss']
})
export class AddUserDialogComponent implements OnInit {

  userForm: FormGroup;

  createForm() {

    const form: any = {
      name: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      email: ['', Validators.compose([Validators.email, Validators.required])]
    };

    this.userForm = this.fb.group(form);
  }

  constructor(
    private fb: FormBuilder, private ms: MealService,
    public dialogRef: MatDialogRef<AddUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.createForm();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

  }

}
