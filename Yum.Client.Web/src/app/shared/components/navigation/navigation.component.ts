import { Component, OnInit, Input } from '@angular/core';
import { faYenSign } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @Input() links: [];

  faYenSign = faYenSign;

  constructor() { }

  ngOnInit() {
  }

}
