import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MealService } from '../../services/meal.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-update-meal-dialog',
  templateUrl: './update-meal-dialog.component.html',
  styleUrls: ['./update-meal-dialog.component.scss']
})
// tslint:disable-next-line: component-class-suffix
export class UpdateMealDialog implements OnInit {

  mealForm: FormGroup;
  isLoading = true;
  date;
  time;
  createForm() {

    const form: any = {
      id: [this.data.id],
      name: ['', Validators.compose([Validators.required, Validators.maxLength(50)])],
      calories: ['', Validators.compose([Validators.min(1), Validators.max(99999), Validators.required])],
      date: ['', Validators.compose([Validators.required])],
      time: ['', Validators.compose([Validators.required])]
    };

    this.mealForm = this.fb.group(form);
  }

  constructor(
    private fb: FormBuilder, private ms: MealService,
    public dialogRef: MatDialogRef<UpdateMealDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.createForm();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.ms.getById(this.data.id, this.data.userId)
      .subscribe(meal => {
        this.mealForm.setValue({
          id: meal.id,
          name: meal.name,
          calories: meal.calories,
          date: meal.dateTime,
          time: moment(meal.dateTime).format('HH:mm')
        });
        this.isLoading = false;
      });
  }

}
