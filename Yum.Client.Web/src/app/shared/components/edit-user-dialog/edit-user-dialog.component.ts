import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MealService } from 'src/app/shared/services/meal.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UpdateMealDialog } from 'src/app/shared/components/update-meal-dialog/update-meal-dialog.component';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.scss']
})
export class EditUserDialogComponent implements OnInit {

  userForm: FormGroup;
  isLoading = true;
  date;
  time;
  createForm() {

    const form: any = {
      id: [this.data.id],
      name: ['', Validators.compose([Validators.required, Validators.maxLength(50)])]
    };

    this.userForm = this.fb.group(form);
  }

  constructor(
    private fb: FormBuilder, private us: UserService,
    public dialogRef: MatDialogRef<EditUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.createForm();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.us.getPrivacyDataByUserId(this.data.id)
      .subscribe(user => {
        this.userForm.setValue({
          id: this.data.id,
          name: user.fullName
        });
        this.userForm.markAsPristine();
        this.isLoading = false;
      });
  }
}
