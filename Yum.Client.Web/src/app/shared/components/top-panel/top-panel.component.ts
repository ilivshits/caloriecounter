import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-top-panel',
  templateUrl: './top-panel.component.html',
  styleUrls: ['./top-panel.component.scss']
})
export class TopPanelComponent implements OnInit {

  userName = '';

  constructor(private as: AuthService, private router: Router, private ud: UserService) { }

  ngOnInit() {
    this.ud.getPrivacyData()
      .subscribe(res => {
        this.userName = res.fullName;
      });
  }

  logout() {
    this.as.logout();
  }

  get firstLetterName() {
    return this.userName ? this.userName[0] : 'O';
  }

}
