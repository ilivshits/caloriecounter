import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { User } from 'src/app/shared/models/user';
import { AuthService } from 'src/app/auth/services/auth.service';
import { AddUserDialogComponent } from '../add-user-dialog/add-user-dialog.component';
import { MatDialog } from '@angular/material';
import { EditUserDialogComponent } from '../edit-user-dialog/edit-user-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit {

  @Input() roles: string[];
  @Input() roleName: string[];
  @Input() canViewMeals = false;

  isLoading = true;
  users: User[] = [];
  displayedColumns: string[] = ['name', 'email', 'actions'];

  constructor(private us: UserService, private as: AuthService, public dialog: MatDialog, public router: Router) { }

  ngOnInit() {
    this.loadUsers();
  }

  loadUsers() {
    this.isLoading = true;
    this.us.getUsers(this.roles)
    .subscribe(users => {
      this.users = users;
      this.isLoading = false;
    });
  }

  onReload() {
    this.loadUsers();
  }

  openAddUserDialog() {
    const dialogRef = this.dialog.open(AddUserDialogComponent, {
      width: '350px',
      data: {role: this.roleName}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.as.signup(result.email, 'qwerty', result.name, this.roles[0])
          .subscribe(res => {
            this.loadUsers();
          });
      }
    });
  }

  openUpdateUserDialog(id) {
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      width: '350px',
      data: {role: this.roleName, id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.us.updateData(result.name, id)
          .subscribe(res => {
            this.loadUsers();
          });
      }
    });
  }

  deleteUser(id) {
    if (confirm('Are you sure want to delete user?')) {
      this.us.delete(id)
        .subscribe(res => {
          this.loadUsers();
        });
    }
  }

  resetPassword(email) {
    if (confirm(`Send reset password link to: ${email}`)) {
      this.as.resetPasswordRequest(email)
        .subscribe(res => {
          alert('Link\'s been sent!');
        });
    }
  }

  viewMeals(userId) {
    this.router.navigate(['admin', userId, 'meals']);
  }
}
