
export function redirectToRootAuthenticated(role, router) {
  let route = [];
  switch (role) {
    case '3':
      route = ['dashboard'];
      break;
    case '1':
      route = ['admin'];
      break;
    case '2':
      route = ['management'];
      break;
  }

  router.navigate(route);
}
