import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatIconModule, MatListModule, MatSidenavModule, MatToolbarModule, MatTableModule,
  MatProgressSpinnerModule, MatDialogModule, MatButtonModule, MatPaginatorModule} from '@angular/material';
import { YumButtonComponent } from './components/yum-button/yum-button.component';
import { DateTimePipe } from './pipes/date-time.pipe';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './services/user.service';
import { MealService } from './services/meal.service';
import { MealsListComponent } from './components/meals-list/meals-list.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TopPanelComponent } from './components/top-panel/top-panel.component';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import { AddMealDialog } from './components/add-meal-dialog/add-meal-dialog.component';
import { UpdateMealDialog } from './components/update-meal-dialog/update-meal-dialog.component';
import { UsersTableComponent } from './components/users-table/users-table.component';

@NgModule({
  declarations: [NavigationComponent, YumButtonComponent, DateTimePipe, MealsListComponent, TopPanelComponent,
    UsersTableComponent],
  imports: [
    OverlayPanelModule,
    FontAwesomeModule,
    RouterModule,
    CommonModule,
    MatPaginatorModule,
    MatButtonModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    HttpClientModule
  ],
  entryComponents: [AddMealDialog, UpdateMealDialog, UsersTableComponent],
  exports: [
    NavigationComponent,
    YumButtonComponent,
    MealsListComponent,
    TopPanelComponent,
    UsersTableComponent
  ],
  providers: [
    MealService,
    UserService,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule
  ]
})
export class SharedModule { }
