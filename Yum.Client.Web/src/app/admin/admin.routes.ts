import { Routes } from '@angular/router';
import { AdminUsersComponent } from './components/admin-users/admin-users.component';
import { ManagersComponent } from './components/managers/managers.component';
import { UsersComponent } from './components/users/users.component';
import { UserMealsComponent } from './components/user-meals/user-meals.component';

export const ADMIN_ROUTES: Routes = [
  { path: '', component: AdminUsersComponent },
  { path: 'managers', component: ManagersComponent },
  { path: 'users', component: UsersComponent },
  { path: ':userId/meals', component: UserMealsComponent }
];
