import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './components/admin/admin.component';
import { MatSidenavModule, MatTableModule, MatProgressSpinnerModule, MatIconModule, MatButtonModule,
  MatDialogModule, MatFormFieldModule, MatTooltipModule, MatInputModule, MatExpansionModule,
  MatNativeDateModule, MatCardModule, MatDatepickerModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { AdminUsersComponent } from './components/admin-users/admin-users.component';
import { UsersTableComponent } from '../shared/components/users-table/users-table.component';
import { AddUserDialogComponent } from '../shared/components/add-user-dialog/add-user-dialog.component';
import { EditUserDialogComponent } from '../shared/components/edit-user-dialog/edit-user-dialog.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ManagersComponent } from './components/managers/managers.component';
import { UsersComponent } from './components/users/users.component';
import { UserMealsComponent } from './components/user-meals/user-meals.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

@NgModule({
  declarations: [
    AdminComponent,
    AdminUsersComponent,
    AddUserDialogComponent,
    EditUserDialogComponent,
    ManagersComponent,
    UsersComponent,
    UserMealsComponent],
  imports: [
    MatDatepickerModule,
    MatCardModule,
    FormsModule,
    MatExpansionModule,
    NgxMaterialTimepickerModule,
    MatNativeDateModule,
    RouterModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    SharedModule,
    MatSidenavModule,
    MatTableModule,
    MatProgressSpinnerModule,
    CommonModule,
    MatInputModule
  ],
  entryComponents: [AddUserDialogComponent, EditUserDialogComponent]
})
export class AdminModule { }
