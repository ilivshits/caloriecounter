import { Component, OnInit } from '@angular/core';
import { faYenSign } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  faYenSign = faYenSign;
  userName = '';
  navLinks = [
    {path: '/admin', label: 'ADMINS', icon: 'panorama_fish_eye'},
    {path: 'managers', label: 'USER MANAGERS', icon: 'supervised_user_circle'},
    {path: 'users', label: 'USERS', icon: 'people'}
  ];


  constructor() { }

  ngOnInit() {
  }

}
