import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Meal } from 'src/app/dashboard/models/meal.model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MealService } from 'src/app/shared/services/meal.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-meals',
  templateUrl: './user-meals.component.html',
  styleUrls: ['./user-meals.component.scss']
})
export class UserMealsComponent implements OnInit {

  meals: Meal[] = [];
  isLoadingMeals = false;
  mealForm: FormGroup;
  userId: string;

  createForm() {
    const today = new Date();
    const form: any = {
      dateFrom: [today, Validators.compose([Validators.required])],
      dateTo: [today, Validators.compose([Validators.required])],
      timeFrom: ['00:00', Validators.compose([Validators.required])],
      timeTo: ['23:59', Validators.compose([Validators.required])]
    };

    this.mealForm = this.fb.group(form);
  }

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder, private ms: MealService) {
    this.createForm();
  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      this.userId = data.userId;
    });
  }

  loadMeals(dateFrom, dateTo, timeFrom, timeTo) {
    this.isLoadingMeals = true;

    this.ms.get(dateFrom, dateTo, timeFrom, timeTo, this.userId)
      .subscribe(meals => {
        this.meals = meals.sort((a, b) => {
          const momentA = moment(a.dateTime);
          const momentB = moment(b.dateTime);
          if (momentA > momentB) {
            return 1;
          } else if (momentA < momentB) {
            return -1;
          } else {
            return 0;
          }});
        this.isLoadingMeals = false;
      });
  }

  search(dateFrom, dateTo, timeFrom, timeTo) {
    this.loadMeals(
      this.dateToNoTzString(dateFrom),
      this.dateToNoTzString(dateTo),
      `${timeFrom}:00`, `${timeTo}:59`);
  }

  dateToNoTzString(date) {
    return moment(date).format(`YYYY-MM-DD HH:mm:ss`);
  }

}
