import { Component, OnInit } from '@angular/core';
import { MealService } from 'src/app/shared/services/meal.service';
import { Meal } from '../../models/meal.model';
import * as moment from 'moment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  meals: Meal[] = [];
  isLoadingMeals = false;
  mealForm: FormGroup;

  createForm() {
    const today = new Date();
    const form: any = {
      dateFrom: [today, Validators.compose([Validators.required])],
      dateTo: [today, Validators.compose([Validators.required])],
      timeFrom: ['00:00', Validators.compose([Validators.required])],
      timeTo: ['23:59', Validators.compose([Validators.required])]
    };

    this.mealForm = this.fb.group(form);
  }

  constructor(
    private fb: FormBuilder, private ms: MealService) {
    this.createForm();
  }

  ngOnInit() {
  }

  loadMeals(dateFrom, dateTo, timeFrom, timeTo) {
    this.isLoadingMeals = true;

    this.ms.get(dateFrom, dateTo, timeFrom, timeTo)
      .subscribe(meals => {
        this.meals = meals.sort((a, b) => {
          const momentA = moment(a.dateTime);
          const momentB = moment(b.dateTime);
          if (momentA > momentB) {
            return 1;
          } else if (momentA < momentB) {
            return -1;
          } else {
            return 0;
          }});
        this.isLoadingMeals = false;
      });
  }

  search(dateFrom, dateTo, timeFrom, timeTo) {
    this.loadMeals(
      this.dateToNoTzString(dateFrom),
      this.dateToNoTzString(dateTo),
      `${timeFrom}:00`, `${timeTo}:59`);
  }

  dateToNoTzString(date) {
    return moment(date).format(`YYYY-MM-DD HH:mm:ss`);
  }
}
