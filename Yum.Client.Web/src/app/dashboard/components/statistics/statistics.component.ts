import { Component, OnInit } from '@angular/core';
import { MealService } from '../../../shared/services/meal.service';
import { Meal } from '../../models/meal.model';
import { UserSettingsService } from '../../services/user-settings.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { AddMealDialog } from '../../../shared/components/add-meal-dialog/add-meal-dialog.component';
import { MatDialog } from '@angular/material';
import { UpdateMealDialog } from '../../../shared/components/update-meal-dialog/update-meal-dialog.component';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  meals: Meal[] = [];
  isLoadingMeals = true;
  mealsLength = 0;
  displayedColumns: string[] = ['name', 'cals', 'date', 'actions'];

  toBeAddedMeal = false;

  maxCaloriesPerDay = 0;
  isLoadingSettings = true;
  circleOuterColorOk = '#0e8100';
  circleOuterColorOver = 'red';
  circleBackgroundColorOk = '#beffb6';
  circleBackgroundColorOver = '#ffe5e5';

  addMealForm: FormGroup;



  constructor(private ms: MealService, private uss: UserSettingsService) {
  }

  ngOnInit() {
    this.loadCaloriesPlan();
    this.loadMeals();
  }

  loadCaloriesPlan() {
    this.isLoadingSettings = true;

    this.uss.getAll()
      .subscribe(setting => {
        if (setting) {
          this.maxCaloriesPerDay = +setting.caloriesPlan;
        }
        this.isLoadingSettings = false;
      });
  }

  loadMeals() {
    this.isLoadingMeals = true;

    const date = moment(new Date()).format(`YYYY-MM-DD HH:mm:ss`);

    this.ms.get(date, date)
      .subscribe(meals => {
        this.meals = meals.sort((a, b) => {
          const momentA = moment(a.dateTime);
          const momentB = moment(b.dateTime);
          if (momentA > momentB) {
            return 1;
          } else if (momentA < momentB) {
            return -1;
          } else {
            return 0;
          }});
        this.mealsLength = this.meals.length;
        this.isLoadingMeals = false;
      });
  }

  get totalCals(): number {
    return this.meals.reduce((a, b) => a + (b.calories || 0), 0);
  }

  get calPercent() {
    return this.totalCals / this.maxCaloriesPerDay * 100;
  }

  get counterTitle() {
    if (this.maxCaloriesPerDay) {
      return [this.maxCaloriesPerDay - this.totalCals];
    } else {
      return ['SET LIMIT', 'IN SETTINGS'];
    }
  }

  get calsLeft() {
    return this.maxCaloriesPerDay - this.totalCals;
  }

  get circleOuterColor() {
    return this.calsLeft < 0 ? this.circleOuterColorOver : this.circleOuterColorOk;
  }

  get circleBackColor() {
    return this.calsLeft < 0 ? this.circleBackgroundColorOver : this.circleBackgroundColorOk;
  }
}
