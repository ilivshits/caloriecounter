import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Meal } from '../models/meal.model';
import { UserSetting } from '../models/user-setting.model';

@Injectable({
  providedIn: 'root'
})
export class UserSettingsService {
  private static USER_SETTINGS_API = 'https://localhost:44385/api/usersettings/';

  constructor(private http: HttpClient) { }

  getAll(): Observable<UserSetting> {
    return this.http.get<UserSetting>(`${UserSettingsService.USER_SETTINGS_API}`);
  }

  set(setting: UserSetting): Observable<any> {
    return this.http.put<any>(`${UserSettingsService.USER_SETTINGS_API}`, {
      caloriesPlan: setting.caloriesPlan
    });
  }
}
