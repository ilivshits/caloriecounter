import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/services/auth.service';
import { faYenSign } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  faYenSign = faYenSign;
  userName = '';
  navLinks = [
    {path: '/dashboard', label: 'TODAY', icon: 'panorama_fish_eye'},
    {path: 'history', label: 'HISTORY', icon: 'history'}
  ];

  constructor(private as: AuthService, private router: Router, private ud: UserService) { }

  ngOnInit() {
    this.ud.getPrivacyData()
      .subscribe(res => {
        this.userName = res.fullName;
      });
  }

  logout() {
    this.as.logout();
  }

  get firstLetterName() {
    return this.userName ? this.userName[0] : 'O';
  }
}
