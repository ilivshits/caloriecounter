export class Meal {
  id: string;
  userId: string;
  dateTime: string;
  calories: number;
  name: string;
}
