export class UserSetting {
  id: string;
  userId: string;
  caloriesPlan: number;
}
