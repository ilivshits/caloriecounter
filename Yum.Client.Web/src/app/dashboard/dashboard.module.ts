import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { StatisticsComponent } from './components/statistics/statistics.component';
import { RouterModule } from '@angular/router';
import { MatListModule, MatSidenavModule,
  MatButtonModule, MatIconModule,
  MatDatepickerModule, MatProgressSpinnerModule,
  MatCardModule, MatInputModule, MatTableModule, MatDialogModule, MatNativeDateModule, MatExpansionModule } from '@angular/material';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import { MealService } from '../shared/services/meal.service';
import { UserSettingsService } from './services/user-settings.service';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AddMealDialog } from '../shared/components/add-meal-dialog/add-meal-dialog.component';
import { UpdateMealDialog } from '../shared/components/update-meal-dialog/update-meal-dialog.component';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { SettingsComponent } from './components/settings/settings.component';
import { HistoryComponent } from './components/history/history.component';

@NgModule({
  declarations: [
    DashboardComponent,
    StatisticsComponent,
    AddMealDialog,
    UpdateMealDialog,
    SettingsComponent,
    HistoryComponent
  ],
  imports: [
    FormsModule,
    MatExpansionModule,
    NgxMaterialTimepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatTableModule,
    ReactiveFormsModule,
    MatCardModule,
    NgCircleProgressModule.forRoot(),
    MatProgressSpinnerModule,
    MatDatepickerModule,
    OverlayPanelModule,
    MatIconModule,
    FontAwesomeModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatInputModule,
    CommonModule,
    SharedModule,
    RouterModule
  ],
  providers: [UserSettingsService],
  exports: [DashboardComponent]
})
export class DashboardModule { }
