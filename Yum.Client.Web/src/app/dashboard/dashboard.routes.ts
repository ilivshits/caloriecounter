import { StatisticsComponent } from './components/statistics/statistics.component';
import { Routes } from '@angular/router';
import { AuthGuard } from '../auth/guards/auth.guard';
import { SettingsComponent } from './components/settings/settings.component';
import { HistoryComponent } from './components/history/history.component';

export const DASHBOARD_ROUTES: Routes = [
  { path: '', component: StatisticsComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'history', component: HistoryComponent },
];
