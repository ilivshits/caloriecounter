import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { faYenSign } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../auth/services/auth.service';
import { ConfirmPasswordStateMatcher } from 'src/app/shared/services/confirm-pswword.state-matcher';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  faYenSign = faYenSign;
  form: FormGroup;
  resetKey: string;
  userId: string;
  matcher = new ConfirmPasswordStateMatcher();

  constructor(private fb: FormBuilder, private as: AuthService, private router: Router, private route: ActivatedRoute) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      password: ['', Validators.compose([Validators.minLength(8), Validators.required])],
      password2: ['', Validators.compose([Validators.minLength(8), Validators.required])]
    }, {validator: this.checkPasswords});
  }

  ngOnInit() {
    this.createForm();
    this.userId = this.route.snapshot.paramMap.get('id');
    this.resetKey = this.route.snapshot.queryParams.resetkey;
    console.log(this.userId);
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.get('password').value;
    const confirmPass = group.get('password2').value;

    return pass === confirmPass ? null : { notSame: true };
  }

  reset(password) {
    this.as.resetPassword(this.userId, password, this.resetKey)
      .subscribe(res => {
        this.as.logout();
        this.router.navigate(['login']);
      });
  }

}
