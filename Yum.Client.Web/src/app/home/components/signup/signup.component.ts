import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { faYenSign } from '@fortawesome/free-solid-svg-icons';
import { ConfirmPasswordStateMatcher } from 'src/app/shared/services/confirm-pswword.state-matcher';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Router } from '@angular/router';
import { redirectToRootAuthenticated } from 'src/app/shared/helpers/utils';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  faYenSign = faYenSign;
  form: FormGroup;
  matcher = new ConfirmPasswordStateMatcher();
  userExists: boolean;

  constructor(private fb: FormBuilder, private as: AuthService, private router: Router) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.compose([Validators.minLength(8), Validators.required])],
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.minLength(8), Validators.required])],
      password2: ['', Validators.compose([Validators.minLength(8), Validators.required])]
    }, {validator: this.checkPasswords});
  }

  signup(name, email, password) {
    this.as.signup(email, password, name)
      .subscribe(res => {
        this.as.login(email, password)
          .subscribe(() => {
            this.router.navigate(['dashboard']);
          }, err => {
          });
      }, err => {
        if (err.status === 409) {
          this.userExists = true;
        }
      });
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.get('password').value;
    const confirmPass = group.get('password2').value;

    return pass === confirmPass ? null : { notSame: true };
  }

  ngOnInit() {
    this.as.isAuthenticated().subscribe(res => {
      if (res) {
        redirectToRootAuthenticated(this.as.getRole().toLowerCase(), this.router);
      }
    });
  }

}
