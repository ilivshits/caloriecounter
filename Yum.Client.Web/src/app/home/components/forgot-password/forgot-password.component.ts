import { Component, OnInit } from '@angular/core';
import { faYenSign } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfirmPasswordStateMatcher } from 'src/app/shared/services/confirm-pswword.state-matcher';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  faYenSign = faYenSign;
  form: FormGroup;
  matcher = new ConfirmPasswordStateMatcher();

  constructor(private fb: FormBuilder, private as: AuthService, private router: Router) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      email: ['', Validators.compose([Validators.email, Validators.required])]
    });
  }

  ngOnInit() {
    this.createForm();
  }

  reset(email) {
    this.as.resetPasswordRequest(email)
      .subscribe(res => {
        this.as.logout();
        this.router.navigate(['login']);
      });
  }

}
