import { Component, OnInit } from '@angular/core';
import { faYenSign } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Router } from '@angular/router';
import { redirectToRootAuthenticated } from 'src/app/shared/helpers/utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  faYenSign = faYenSign;
  form: FormGroup;
  wrongLogin: boolean;

  constructor(private fb: FormBuilder, private as: AuthService, private router: Router) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      password: ['', Validators.compose([Validators.minLength(8), Validators.required])]
    });
  }

  login(email, password) {
    this.as.login(email, password)
      .subscribe(res => {
        this.router.navigate(['']);
      }, err => {
        this.wrongLogin = true;
      });
  }

  ngOnInit() {
    this.as.isAuthenticated().subscribe(res => {
      if (res) {
        redirectToRootAuthenticated(this.as.getRole().toLowerCase(), this.router);
      }
    });
  }

}
