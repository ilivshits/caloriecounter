import { Component, OnInit } from '@angular/core';
import { faYenSign } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../auth/services/auth.service';
import { Routes, Router } from '@angular/router';
import { redirectToRootAuthenticated } from '../shared/helpers/utils';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  faYenSign = faYenSign;

  constructor(private as: AuthService, private router: Router) { }

  ngOnInit() {
    this.as.isAuthenticated().subscribe(res => {
      if (res) {
        redirectToRootAuthenticated(this.as.getRole().toLowerCase(), this.router);
      }
    });
  }

  navigate(path) {
    this.router.navigate(path);
  }

}
