import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import {JwtModule} from '@auth0/angular-jwt';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from './shared/shared.module';
import { AuthModule } from './auth/auth.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { SignupComponent } from './home/components/signup/signup.component';
import { LoginComponent } from './home/components/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule, MatButtonModule } from '@angular/material';
import { ResetPasswordComponent } from './home/components/reset-password/reset-password.component';
import { ForgotPasswordComponent } from './home/components/forgot-password/forgot-password.component';
import { AdminModule } from './admin/admin.module';
import { ManagementModule } from './management/management.module';

export function tokenGetter() {
  return localStorage.getItem('yum_access_token');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent
  ],
  imports: [
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    DashboardModule,
    BrowserModule,
    AuthModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    SharedModule,
    AdminModule,
    ManagementModule,
    JwtModule.forRoot({
      config: {
        tokenGetter
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
