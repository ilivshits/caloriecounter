import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagementComponent } from './components/management/management.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule, MatNativeDateModule, MatTooltipModule, MatFormFieldModule,
  MatDialogModule, MatIconModule, MatButtonModule, MatSidenavModule, MatTableModule,
  MatProgressSpinnerModule, MatInputModule, MatCardModule } from '@angular/material';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { UsersComponent } from './components/users/users.component';
import { SettingsComponent } from './components/settings/settings.component';



@NgModule({
  declarations: [ManagementComponent,
    SettingsComponent, UsersComponent],
  imports: [
    MatCardModule,
    FormsModule,
    MatExpansionModule,
    NgxMaterialTimepickerModule,
    MatNativeDateModule,
    RouterModule,
    MatTooltipModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    SharedModule,
    MatSidenavModule,
    MatTableModule,
    MatProgressSpinnerModule,
    CommonModule,
    MatInputModule
  ]
})
export class ManagementModule { }
