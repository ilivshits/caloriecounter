import { Routes } from '@angular/router';
import { UsersComponent } from '../management/components/users/users.component';
import { SettingsComponent } from './components/settings/settings.component';

export const USER_MANAGER_ROUTES: Routes = [
  { path: '', component: UsersComponent },
  { path: 'settings', component: SettingsComponent }
];
