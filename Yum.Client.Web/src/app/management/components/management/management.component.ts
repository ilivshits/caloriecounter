import { Component, OnInit } from '@angular/core';
import { faYenSign } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss']
})
export class ManagementComponent implements OnInit {

  faYenSign = faYenSign;
  userName = '';
  navLinks = [
    {path: '/management', label: 'Users', icon: 'panorama_fish_eye'}
  ];

  constructor() { }

  ngOnInit() {

  }

}
