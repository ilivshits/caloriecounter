import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserPrivacyData } from 'src/app/shared/models/user-privacy-data';
import { UserService } from 'src/app/shared/services/user.service';
import { combineLatest } from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';
import { UserSettingsService } from 'src/app/dashboard/services/user-settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  userName: string;
  privacyForm: FormGroup;
  loadingPersonalData = true;
  resetPasswordConfirm = false;
  resetPasswordConfirmed = false;

  constructor(private fb: FormBuilder, private us: UserSettingsService, private ud: UserService, private as: AuthService) {
    this.privacyForm = this.fb.group({
      userNameControl: ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.loadPersonalData();
  }

  loadPersonalData() {
    this.loadingPersonalData = true;
    this.ud.getPrivacyData()
      .subscribe(userPrivacy => {

        if (userPrivacy) {
          this.userName = userPrivacy.fullName;
        }

        this.loadingPersonalData = false;
        this._markFormPristine(this.privacyForm);
      },
      err => console.error(err),
    );
  }

  savePersonalData() {

    const requests = [];

    if (this.privacyForm.controls.userNameControl.dirty) {
      requests.push(this.ud.updateData(this.userName));
    }

    if (requests.length) {
      this.loadingPersonalData = true;

      combineLatest(requests)
        .subscribe(res => {
          this.loadPersonalData();
        });
    }
  }

  resetPassword() {
    this.as.resetPasswordRequest()
      .subscribe(res => {
        this.resetPasswordConfirm = false;
        this.resetPasswordConfirmed = true;
      });
  }

  resetPasswordAsk() {
    this.resetPasswordConfirm = true;
  }

  private _markFormPristine(form: FormGroup): void {
    Object.keys(form.controls).forEach(control => {
        form.controls[control].markAsPristine();
    });
}
}
