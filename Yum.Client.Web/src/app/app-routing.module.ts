import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './home/components/login/login.component';
import { SignupComponent } from './home/components/signup/signup.component';
import { AuthGuard } from './auth/guards/auth.guard';
import {AuthModule} from './auth/auth.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DASHBOARD_ROUTES } from './dashboard/dashboard.routes';
import { ResetPasswordComponent } from './home/components/reset-password/reset-password.component';
import { ForgotPasswordComponent } from './home/components/forgot-password/forgot-password.component';
import { AdminComponent } from './admin/components/admin/admin.component';
import { ADMIN_ROUTES } from './admin/admin.routes';
import { ManagementComponent } from './management/components/management/management.component';
import { USER_MANAGER_ROUTES } from './management/management.routes';
import { RoleGuard } from './shared/guards/role.guard';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'reset-password/:id', component: ResetPasswordComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'signup', component: SignupComponent},
  { path: 'dashboard', component: DashboardComponent,
    children: DASHBOARD_ROUTES, canActivate: [AuthGuard], canActivateChild: [RoleGuard], data: {roles: ['3']}},
  { path: 'admin', component: AdminComponent,
    children: ADMIN_ROUTES, canActivate: [AuthGuard], canActivateChild: [RoleGuard], data: {roles: ['1']}},
  { path: 'management', component: ManagementComponent,
    children: USER_MANAGER_ROUTES, canActivate: [AuthGuard], canActivateChild: [RoleGuard], data: {roles: ['2']}}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    AuthModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
