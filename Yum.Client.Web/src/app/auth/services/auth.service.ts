import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable, Subject, of, BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/do';
import {catchError, map} from 'rxjs/operators';
import 'rxjs/add/Observable/of';
import { User } from '../models/user';
import { Token } from '../models/token';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private static AUTH_API = 'https://localhost:44303/api/auth/';

  constructor(private http: HttpClient, private jwtHelper: JwtHelperService, private router: Router) { }

  signup(email: string, password: string, username: string, role: string = null): Observable<User> {
    return this.http.post<User>(`${AuthService.AUTH_API}signup`, {
      email,
      password,
      username,
      role
    });
  }

  login(email: string, password: string): Observable<Token> {
    return this.http.post<Token>(`${AuthService.AUTH_API}login`, {
      email,
      password
    })
    .do(token => {
      this.updateTokens(token.accessToken, token.refreshToken, token.role);
    });
  }

  refreshToken(accessToken: string, refreshToken: string): Observable<Token> {
    return this.http.post<Token>(`${AuthService.AUTH_API}refresh`, {
      accessToken,
      refreshToken
    })
    .do(token => {
      this.updateTokens(token.accessToken, token.refreshToken, token.role);
    });
  }

  logout(): void {
    localStorage.removeItem('yum_access_token');
    localStorage.removeItem('yum_refresh_token');
    localStorage.removeItem('yum_user_role');

    this.router.navigate(['']);
  }

  isAuthenticated(ifNot = null): Observable<boolean> {
    const subject: Subject<boolean> = new Subject<boolean>();

    const token = localStorage.getItem('yum_access_token');
    const refreshToken = localStorage.getItem('yum_refresh_token');

    if (token && !this.jwtHelper.isTokenExpired(token)) {
      return Observable.of(true);
      //subject.next(true);
    } else {
      if (token && refreshToken) {
        return this.refreshToken(token, refreshToken)
        .pipe(
          map((t: Token) => {
            this.updateTokens(t.accessToken, t.refreshToken, t.role);
            return true;
          }),
          catchError(err => {
            return Observable.of(false);
          })
        );
          // .subscribe(t => {
          //   this.updateTokens(t.accessToken, t.refreshToken, t.role);
          //   subject.next(true);
          // }, err => {
          //   if (err.status === 401) {
          //   }
          //   this.logout();
          //   this.router.navigate(['']);
          //   subject.next(false);
          // });
      } else {
        return Observable.of(false);
        if (ifNot) { ifNot(); }
      }
    }
  }

  updateTokens(accessToken: string, refreshToken: string, role: string) {
    localStorage.setItem('yum_access_token', accessToken);
    localStorage.setItem('yum_refresh_token', refreshToken);
    localStorage.setItem('yum_user_role', role);
  }

  getToken(): Token {
    const token = new Token();
    token.accessToken = localStorage.getItem('yum_access_token');
    token.refreshToken = localStorage.getItem('yum_refresh_token');

    if (!token.accessToken || !token.refreshToken) {
      return null;
    }

    return token;
  }

  getRole(): string {
    return localStorage.getItem('yum_user_role');
  }

  resetPasswordRequest(email = null): Observable<any> {
    return this.http.post(`${AuthService.AUTH_API}reset-password`, {
      email
    });
  }

  resetPassword(id, password, resetKey): Observable<any> {
    return this.http.put(`${AuthService.AUTH_API}reset-password/${id}`, {
      password, resetKey
    });
  }
}
