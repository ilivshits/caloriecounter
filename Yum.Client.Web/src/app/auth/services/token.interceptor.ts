import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, EMPTY, Subject } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, switchMap } from 'rxjs/operators';
import { empty } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {

  refreshTokenInProgress = false;

  tokenRefreshedSource = new Subject();
  tokenRefreshed$ = this.tokenRefreshedSource.asObservable();

  constructor(private as: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // let token = this.as.getToken();

    // let newHeaders = req.headers;
    // if (token) {
    //     newHeaders = newHeaders.append('Authorization', `Bearer ${token.accessToken}`);
    // }

    let authReq = this.addAuthHeader(request);
    return next.handle(authReq).pipe(catchError(err => {

      return this.refreshToken().pipe(
        switchMap(() => {
          authReq = this.addAuthHeader(authReq);
          return next.handle(authReq);
        }),
        catchError(() => {
          this.as.logout();
          return EMPTY;
        }));
    }));
  }

  refreshToken() {
    if (this.refreshTokenInProgress) {
        return new Observable(observer => {
            this.tokenRefreshed$.subscribe(() => {
                observer.next();
                observer.complete();
            });
        });
    } else {
        this.refreshTokenInProgress = true;
        const token = this.as.getToken();
        return this.as.isAuthenticated()
           .do(() => {
                this.refreshTokenInProgress = false;
                this.tokenRefreshedSource.next();
            });
    }
}

  addAuthHeader(request) {
    const token = this.as.getToken();
    if (token) {
        return request.clone({
            setHeaders: {
                'Authorization': `Bearer ${token.accessToken}`
            }
        });
    }
    return request;
  }
}
