import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';
import { Observable, Subject } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { map } from 'rxjs-compat/operator/map';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private jwtHelper: JwtHelperService,
    private router: Router,
    private as: AuthService) {
  }

  canActivate(): Observable<boolean> {
    return this.as.isAuthenticated()
    .map(e => {
      if (e) {
        return true;
      } else {
        return false;
      }
    });
  }
}
