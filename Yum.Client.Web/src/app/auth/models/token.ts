export class Token {
  accessToken: string;
  refreshToken: string;
  role: string;
}
