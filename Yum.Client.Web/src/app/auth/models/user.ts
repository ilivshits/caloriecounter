export class User {
  id: string;
  email: string;
  name: string;
  role: number;
}
