﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yum.Auth.Web.Models.Requests
{
    public class ResetPasswordRequest
    {
        public string ResetKey { get; set; }
        public string Password { get; set; }
    }
}
