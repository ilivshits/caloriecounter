﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yum.Auth.Common;

namespace Yum.Auth.Web.Models.Requests
{
    public class SignUpRequest
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Role? Role { get; set; }
    }
}
