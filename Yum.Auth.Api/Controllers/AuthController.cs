﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yum.Auth.Web.Models;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Security.Cryptography;
using Yum.Auth.Web.Models.Requests;
using Yum.Auth.Services;
using Yum.Auth.Services.Models;
using System.Text.Encodings.Web;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace Yum.Auth.Web.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserService _userService;
        private readonly EmailService _emailService;
        private readonly RefreshTokenService _refreshTokenService;
        private readonly IConfiguration _configuration;

        public AuthController(UserService userService, EmailService emailService, RefreshTokenService refreshTokenService, IConfiguration configuration)
        {
            _userService = userService;
            _emailService = emailService;
            _refreshTokenService = refreshTokenService;
            _configuration = configuration;
        }

        // GET api/values
        [HttpPost, Route("login")]
        public IActionResult Login([FromBody]LoginRequest request)
        {
            if (request == null)
            {
                return BadRequest("Body cannot be empty.");
            }

            var user = _userService.Get(request.Email);

            if (user == null) 
                return NotFound("User cannot be found.");

            if (BCrypt.Net.BCrypt.Verify(request.Password, user.Password))
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.Name),
                    new Claim(ClaimTypes.Role, user.Role.ToString()),
                    new Claim(ClaimTypes.Email, user.Email)
                };

                var tokenString = GenerateToken(claims);
                var refreshToken = GenerateRefreshToken();
                SaveRefreshToken(user.Id, refreshToken);

                return new ObjectResult(new
                {
                    AccessToken = tokenString,
                    RefreshToken = refreshToken,
                    Role = user.Role
                });
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost, Route("signup")]
        public IActionResult SignUp([FromBody]SignUpRequest request)
        {
            User user;
            try
            {
                user = _userService.Create(new Services.Models.User
                {
                    Email = request.Email,
                    Name = request.UserName,
                    Password = BCrypt.Net.BCrypt.HashPassword(request.Password),
                    Role = request.Role ?? Common.Role.User
                });
            }
            catch (InvalidOperationException)
            {
                return Conflict("User with such email already exists.");
            }            

            return Ok(user);
        }

        [HttpPost, Route("refresh")]
        public IActionResult Refresh([FromBody]RefreshRequest request)
        {
            var principal = GetPrincipalFromExpiredToken(request.AccessToken);
            var userId = Guid.Parse(principal.FindFirstValue(ClaimTypes.NameIdentifier));
            var savedRefreshTokens = GetRefreshTokens(userId);
            if (!savedRefreshTokens.Contains(request.RefreshToken))
                throw new SecurityTokenException("Invalid refresh token");

            var newJwtToken = GenerateToken(principal.Claims);
            var newRefreshToken = GenerateRefreshToken();
            DeleteRefreshToken(userId, request.RefreshToken);
            SaveRefreshToken(userId, newRefreshToken);

            var role = Enum.Parse<Common.Role>(principal.FindFirstValue(ClaimTypes.Role));

            return new ObjectResult(new
            {
                accessToken = newJwtToken,
                refreshToken = newRefreshToken,
                role = role
            });
        }

        [HttpPost, Route("reset-password")]
        [Authorize]
        [AllowAnonymous]
        public IActionResult ResetPassword([FromBody]ResetPasswordEmailRequest resetRequest)
        {
            var email = User?.FindFirstValue(ClaimTypes.Email);
            
            var user = _userService.Get(resetRequest?.Email ?? email);
            if (user == null) return Ok();

            var secret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes($"{user.Email}{user.Password}"));
            var signinCredentials = new SigningCredentials(secret, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(signingCredentials: signinCredentials));

            string resetPasswordLink = $"{_configuration["ConnectionStrings:Yum.Client.Web"]}reset-password/{user.Id}?resetkey={System.Web.HttpUtility.UrlEncode(token)}";
            
            _emailService.SendEmail(user.Email, "Reset Password Link", resetPasswordLink);

            return Ok();
        }

        [HttpPut, Route("reset-password/{userId}")]
        [Authorize]
        [AllowAnonymous]
        public IActionResult ResetPassword(Guid userId, [FromBody]ResetPasswordRequest resetRequest)
        {
            var user = _userService.Get(userId);

            var secret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes($"{user.Email}{user.Password}"));
            var signinCredentials = new SigningCredentials(secret, SecurityAlgorithms.HmacSha256);
            var resetKeyExpected = new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(signingCredentials: signinCredentials));

            if (resetKeyExpected == resetRequest.ResetKey)
            {
                _userService.UpdatePassword(userId, BCrypt.Net.BCrypt.HashPassword(resetRequest.Password));

                return Ok();
            }
            else
            {
                return Unauthorized("Wrong reset key");
            }
        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, 
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345")),
                ValidateLifetime = false 
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }

        private string GenerateToken(IEnumerable<Claim> claims)
        {
            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

            var jwt = new JwtSecurityToken(
                //issuer: "http://localhost:5020",
                //audience: "http://localhost:5020",
                claims: claims, 
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddSeconds(10),
                signingCredentials: signinCredentials
            );

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        private IEnumerable<string> GetRefreshTokens(Guid id)
        {
            return _refreshTokenService.Get(id).Select(s => s.Token);
        }

        private void DeleteRefreshToken(Guid id, string refreshToken)
        {
            _refreshTokenService.Delete(id, refreshToken);
        }

        private void SaveRefreshToken(Guid id, string refreshToken)
        {
            _refreshTokenService.Create(id, refreshToken);
        }
    }
}