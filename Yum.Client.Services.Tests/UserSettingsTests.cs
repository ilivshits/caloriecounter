//using Microsoft.EntityFrameworkCore;
//using NUnit.Framework;
//using System;
//using System.Linq;
//using Yum.Client.Data;

//namespace Yum.Client.Services.Tests
//{
//    public class UserSettingsTests
//    {
//        private UserSettingsService _userSettingsService;
//        private Func<Context> _contextFactory;

//        [SetUp]
//        public void Setup()
//        {
//            var options = new DbContextOptionsBuilder<Context>()
//                .UseInMemoryDatabase(databaseName: "Yum.Tests")
//                .Options;

//            _contextFactory = () => new Context(options);

//            _userSettingsService = new UserSettingsService(_contextFactory());
//        }

//        [Test]
//        public void Should_Create_User_Settings()
//        {
//            var setting = new Models.UserSetting()
//            {
//                Key = "Setting 1",
//                UserId = Guid.NewGuid(),
//                Value = "some value"
//            };

//            _userSettingsService.CreateOrUpdate(setting);

//            var settings = _userSettingsService.Get(setting.UserId);

//            Assert.AreEqual(1, settings.Count());
//            var settingActual = settings.First();
//            Assert.AreEqual(setting.Key, settingActual.Key);
//            Assert.AreEqual(setting.UserId, settingActual.UserId);
//            Assert.AreEqual(setting.Value, settingActual.Value);
//        }

//        [Test]
//        public void Should_Update_User_Settings()
//        {
//            var setting = new Models.UserSetting()
//            {
//                Key = "Setting 2",
//                UserId = Guid.NewGuid(),
//                Value = "some value 2"
//            };

//            var settingUpdate = new Models.UserSetting()
//            {
//                Key = "Setting 2",
//                UserId = setting.UserId,
//                Value = "some value 2 updated"
//            };

//            _userSettingsService.CreateOrUpdate(setting);
//            _userSettingsService.CreateOrUpdate(settingUpdate);

//            var settings = _userSettingsService.Get(setting.UserId);

//            Assert.AreEqual(1, settings.Count());
//            var settingActual = settings.First();
//            Assert.AreEqual(settingUpdate.Key, settingActual.Key);
//            Assert.AreEqual(settingUpdate.UserId, settingActual.UserId);
//            Assert.AreEqual(settingUpdate.Value, settingActual.Value);
//        }
//    }
//}