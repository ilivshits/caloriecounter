﻿using System;
using System.Collections.Generic;
using System.Linq;
using Yum.Client.Services.Extensions;
using Yum.Client.Services.Models;

namespace Yum.Client.Services
{
    public class MealService
    {
        private Data.Context _mealContext;

        public MealService(Data.Context context)
        {
            _mealContext = context;
        }

        public Meal Create(Meal meal)
        {
            var entity = _mealContext.Meals.Add(new Data.Entities.Meal()
            {
                Id = Guid.NewGuid(),
                Calories = meal.Calories,
                DateTime = meal.DateTime,
                UserId = meal.UserId,
                Name = meal.Name
            });

            _mealContext.SaveChanges();
            return entity.Entity.ToDto();
        }

        public IEnumerable<Meal> Get(Guid[] mealIds)
        {
            var entities = _mealContext.Meals.Where(a => mealIds.Contains(a.Id));

            return entities.Select(a => a.ToDto()).ToList();
        }

        public Meal Get(Guid mealId)
        {
            return Get(new[] { mealId }).FirstOrDefault();
        }

        public IEnumerable<Meal> Get(
            Guid userId,
            DateTimeOffset? dateFrom = null,
            DateTimeOffset? dateTo = null,
            TimeSpan? timeFrom = null,
            TimeSpan? timeTo = null)
        {
            var query = _mealContext.Meals.AsQueryable();

            query = query.Where(a => a.UserId == userId);

            if (dateFrom.HasValue)
            {
                query = query.Where(a => a.DateTime.Date >= dateFrom.Value.Date);
            }

            if (dateTo.HasValue)
            {
                query = query.Where(a => a.DateTime.Date <= dateTo.Value.Date);
            }

            if (timeFrom.HasValue)
            {
                query = query.Where(a => a.DateTime.TimeOfDay > timeFrom.Value);
            }

            if (timeTo.HasValue)
            {
                query = query.Where(a => a.DateTime.TimeOfDay < timeTo.Value);
            }

            return query.Select(a => a.ToDto()).ToList();
        }

        public void Update(Guid id, string name = null, int? calories = null, DateTimeOffset? dateTime = null)
        {
            var entity = _mealContext.Meals.FirstOrDefault(a => a.Id == id);

            if (entity == null)
            {
                throw new InvalidOperationException("Meal with such Id does not exist.");
            }

            entity.Name = name ?? entity.Name;
            entity.Calories = calories ?? entity.Calories;
            entity.DateTime = dateTime ?? entity.DateTime;

            _mealContext.Meals.Update(entity);
            _mealContext.SaveChanges();
        }

        public void Delete(Guid[] mealIds)
        {
            var entities = _mealContext.Meals.Where(a => mealIds.Contains(a.Id));

            _mealContext.RemoveRange(entities);
            _mealContext.SaveChanges();
        }

        public void Delete(Guid mealId)
        {
            Delete(new[] { mealId });
        }
    }
}
