﻿using System;
using System.Collections.Generic;
using System.Text;
using Yum.Client.Services.Models;
using Yum.Client.Data;

namespace Yum.Client.Services.Extensions
{
    public static class MappingExtensions
    {
        public static Meal ToDto(this Data.Entities.Meal entity)
        {
            return new Meal
            {
                Id = entity.Id,
                Calories = entity.Calories,
                DateTime = entity.DateTime,
                Name = entity.Name,
                UserId = entity.UserId
            };
        }
        public static UserSetting ToDto(this Data.Entities.UserSetting entity)
        {
            return new UserSetting
            {
                Id = entity.Id,
                UserId = entity.UserId,
                CaloriesPlan = entity.CaloriesPlan
            };
        }
    }
}
