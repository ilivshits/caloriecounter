﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using Yum.Client.Services.Extensions;
using Yum.Client.Services.Models;

namespace Yum.Client.Services
{
    public class UserSettingsService
    {
        private Data.Context _context;

        public UserSettingsService(Data.Context context)
        {
            _context = context;
        }

        public UserSetting SetCaloriesPlan(UserSetting setting)
        {
            var entity = _context.UserSettings.FirstOrDefault(a => a.UserId == setting.UserId);

            EntityEntry<Data.Entities.UserSetting> newEntity;

            if (entity == null)
            {
                newEntity = _context.UserSettings.Add(new Data.Entities.UserSetting() 
                {
                    Id = Guid.NewGuid(),
                    UserId = setting.UserId,
                    CaloriesPlan = setting.CaloriesPlan
                });
            }
            else
            {
                entity.CaloriesPlan = setting.CaloriesPlan;
                newEntity = _context.UserSettings.Update(entity);
            }

            _context.SaveChanges();
            return newEntity.Entity.ToDto();
        }

        public UserSetting Get(Guid userId, string key)
        {
            var entity = _context.UserSettings.FirstOrDefault(a => a.UserId == userId);

            return entity?.ToDto();
        }

        public UserSetting Get(Guid userId)
        {
            var entity = _context.UserSettings.FirstOrDefault(a => a.UserId == userId);

            return entity?.ToDto();
        }
    }
}
