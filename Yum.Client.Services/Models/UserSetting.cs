﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yum.Client.Services.Models
{
    public class UserSetting
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int? CaloriesPlan { get; set; }
    }
}
