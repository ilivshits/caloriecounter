﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Yum.Client.Data.Migrations
{
    public partial class CaloriesPlanSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_UserSettings_Key_UserId",
                table: "UserSettings");

            migrationBuilder.DropColumn(
                name: "Key",
                table: "UserSettings");

            migrationBuilder.DropColumn(
                name: "Value",
                table: "UserSettings");

            migrationBuilder.AddColumn<int>(
                name: "CaloriesPlan",
                table: "UserSettings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_UserId",
                table: "UserSettings",
                column: "UserId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_UserSettings_UserId",
                table: "UserSettings");

            migrationBuilder.DropColumn(
                name: "CaloriesPlan",
                table: "UserSettings");

            migrationBuilder.AddColumn<string>(
                name: "Key",
                table: "UserSettings",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Value",
                table: "UserSettings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_Key_UserId",
                table: "UserSettings",
                columns: new[] { "Key", "UserId" },
                unique: true,
                filter: "[Key] IS NOT NULL");
        }
    }
}
