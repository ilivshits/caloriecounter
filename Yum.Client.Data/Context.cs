﻿using Microsoft.EntityFrameworkCore;
using System;
using Yum.Client.Data.Entities;

namespace Yum.Client.Data
{
    public class Context : DbContext
    {
        public DbSet<Meal> Meals { get; set; }
        public DbSet<UserSetting> UserSettings { get; set; }

        public Context()
        {

        }

        public Context(DbContextOptions<Context> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserSetting>()
                .HasIndex(a => a.UserId)
                .IsUnique();
        }
    }
}
