﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yum.Client.Data.Entities
{
    public class Meal
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public DateTimeOffset DateTime { get; set; }
        public int Calories { get; set; }
        public string Name { get; set; }
    }
}
