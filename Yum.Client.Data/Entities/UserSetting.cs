﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yum.Client.Data.Entities
{
    public class UserSetting
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int? CaloriesPlan { get; set; }
    }
}
