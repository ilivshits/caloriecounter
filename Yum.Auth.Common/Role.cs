﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yum.Auth.Common
{
    public enum Role
    {
        Admin = 1,
        UserManager = 2,
        User = 3
    }
}
